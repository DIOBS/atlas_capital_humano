.PHONY: build serve watch clean

all: build

enter:
	@docker run --rm -it \
            --volume $$PWD:/root \
            --workdir /root \
            --user $$(id -u):$$(id -g) \
            --publish 8888:8000 \
            registry.gitlab.com/padawanphysicist/nikola \
            bash



test: public/
	@docker run --rm \
            --detach \
            --volume $$PWD:/root \
            --workdir /root \
            --user $$(id -u):$$(id -g) \
            --publish 8888:8000 \
            --name nikola_tmp \
            registry.gitlab.com/padawanphysicist/nikola \
            nikola serve
	-wget --spider --debug --execute robots=off -r -p -l 0 http://localhost:8888/ 2>&1
	@docker stop nikola_tmp

build:
	@docker run --rm \
            --volume $$PWD:/root \
            --workdir /root \
            --user $$(id -u):$$(id -g) \
            registry.gitlab.com/padawanphysicist/nikola \
            nikola build;

serve: build
	@docker run --rm \
            --volume $$PWD:/root \
            --workdir /root \
            --user $$(id -u):$$(id -g) \
            --publish 8888:8000 \
            registry.gitlab.com/padawanphysicist/nikola \
            nikola serve;

watch:
	@docker run --rm \
            --volume $$PWD:/root \
            --workdir /root \
            --user $$(id -u):$$(id -g) \
            --publish 8888:8000 \
            registry.gitlab.com/padawanphysicist/nikola \
            nikola auto --address 0.0.0.0


clean:
	@rm -rf cache/ __pycache__/
